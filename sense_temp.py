from sense_hat import SenseHat
from flask import Flask, render_template

def tempFunc():
    sense = SenseHat()
    temp = round(sense.get_temperature(),3)
    message = 'Temp:%s' %(temp)
    if temp > 7:
        sense.show_message("Ok", scroll_speed=0.1, text_colour=[0,255,0])
    else:
        sense.show_message("ALARM", scroll_speed=0.1, text_colour=[255,255,255], back_colour=[255,0,0])
    sense.clear()
    return message

app = Flask(__name__)

@app.route('/')
def index():
    currentTemp=tempFunc()
    tempData={
        'temp':currentTemp
        }
    return render_template('index.html',**tempData)

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')